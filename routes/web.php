<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin', 'HomeController@admin_page')->name('admin');
Route::get('/admin/users', 'AdminController@admin_users')->name('admin/users');
Route::get('/editor_page','HomeController@edit_info');
Route::post('/upload_profpic', 'HomeController@upload_profpic');
Route::post('/edit', 'Auth\EditController@validator');
Route::post('/getusers', 'AdminController@get_users');
Route::post('/edit_users', 'AdminController@edit_users');
Route::post('/delete_users', 'AdminController@delete_users');

