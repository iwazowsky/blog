@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="home-user-header">
            <h4>{{ Auth::user()->name}} {{Auth::user()->surname }}</h4>
            <span>
                <a href="{{url('/editor_page')}}">
                    <i class="fas fa-user-cog"></i>
                </a>
            </span>
            <div class="prof-pic" style="background-image: url({{ Auth::user()->profpic}});">
                <div class="prof-pic-hover  justify-content-center align-items-center">
                    <form action="/upload_profpic" method="post" id='submit-profpic' enctype="multipart/form-data">
                         {{ csrf_field() }}
                         <div class="file-field input-field ">
                          <div class="btn">
                            <span>{{ __('Select new pic') }}</span>
                            <input type="file" name="profpic" id="profpic-picker">
                          </div>
                          <div class="file-path-wrapper">
                            <input class="file-path validate" type="text">
                          </div>
                        </div>
                        <input type="text" style="display: none" name="user-id" value="{{ Auth::user()->id}}">
                    </form>
                </div>
            </div>
        </div>
    @if (session('status'))
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                </div>
            </div>
        </div>
    @endif
        <div class="make-post col s6 offset-s6">
            <textarea></textarea>
            <div class="file-field input-field">
              <div class="btn">
                <span>File</span>
                <input type="file">
              </div>
              <div class="file-path-wrapper">
                <input class="file-path validate" type="text">
              </div>
            </div>
            <button class="btn send-post">Post</button>
        </div>
        <div class="home-user-posts col s4 offset-s5"></div>
</div>
</div>
            <script>
                let header_length = $('.home-user-header h4').width()
                $('.home-user-header').width(header_length+10)
                $(document).on('change','#profpic-picker',()=>{
                    $('#submit-profpic').submit()
                })
                $('.send-post').click(function() {
                    
                    $.ajax({
                        url:'/send_post',
                        type: 'post',
                        data:{'_token':'<?php echo csrf_token() ?>'},



                    })
                });
            </script>
@endsection
