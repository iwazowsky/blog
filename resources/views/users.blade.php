@extends('layouts.app')
@section('content')

<div class="table-container">
  <table id="example" class="mdl-data-table" style="width:100%">
          <thead>
              <tr>
                  <th>{{__('Id')}}</th>
                  <th>{{__('Name')}}</th>
                  <th>{{__('Surname')}}</th>
                  <th>{{__('Email')}}</th>
                  <th>{{__('Username')}}</th>
                  <th>{{__('Profile picture')}}</th>
                  <th>{{__('Birthdate')}}</th>
                  <th>{{__('Created')}}</th>
                  <th>{{__('Last Updated')}}</th>
                  <th>{{__('Status')}}</th>
                  <th>{{__('Active')}}</th>
                  <th>{{__('Edit')}}</th>
                  <th>{{__('Delete')}}</th>

              </tr>
          </thead>
          <tbody>
          
          </tbody>
          <tfoot>
             <tr>
                  <th>{{__('Id')}}</th>
                  <th>{{__('Name')}}</th>
                  <th>{{__('Surname')}}</th>
                  <th>{{__('Email')}}</th>
                  <th>{{__('Username')}}</th>
                  <th>{{__('Profile picture')}}</th>
                  <th>{{__('Birthdate')}}</th>
                  <th>{{__('Created')}}</th>
                  <th>{{__('Last Updated')}}</th>
                  <th>{{__('Status')}}</th>
                  <th>{{__('Active')}}</th>
                  <th>{{__('Edit')}}</th>
                  <th>{{__('Delete')}}</th>
              </tr>
          </tfoot>
  </table> 
  </div> 
</div>
<script>
  $(document).ready(function() {
    $.ajaxSetup({
      headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    let get_users_ajax = {
             type:'POST',
             url:'/getusers',
             "dataSrc":"",
             data:'_token = <?php echo csrf_token() ?>',
             dataType:'json',
           }
   $('#example').DataTable( {

      'ajax':get_users_ajax,
        columns: [{"data":'id'},{'data':'name'},{'data':'surname'},{'data':'email'},{'data':'username'},{'data':'profpic'},{'data':'birthdate'},{'data':'created_at'},{'data':'updated_at'},{'data':'type'},{'data':'active'},{'data':'edit'},{'data':'delete'}],
      'responsive':true,
        dom: 'Bfrtip',
        buttons: [
        { "extend": 'copy', "text":'Export html',"className": 'btn btn-info' },
        { "extend": 'excel', "text":'Export excel',"className": 'btn btn-info' },  
        { "extend": 'pdf', "text":'Export pdf',"className": 'btn btn-info' }
        ]
    } );


  var table = $('#example')
     $('#sidebarCollapse').on('click', function () {
         $('#sidebar').toggleClass('active');
     });

 $(document).on('click', '.edit', function() {
   $(this).parents('tr').find('td').each( function(){
    if($(this).children().length==0){
      let text = $(this).text()
      $(this).text('')
      $(this).append(`<input type="text">`)
      $(this).find('input').val(text)
    }
   })
    $(this).before('<button class="btn send-edit">Save</button>')
    $(this).remove()
 });
    var obj={}
    $('#example').find('tr').eq(0).find("th").each(function(){
      let string = $(this).html()
      string = string.toLowerCase()
      obj[string]=''
    })
    delete obj.edit
    delete obj.delete
 $(document).on('click', '.send-edit', function() {
      var obj_count = 0
   $(this).parents('tr').find('td').each( function(){
    if($(this).children('input').length!=0){
      let text = $(this).children('input').val()
      obj[Object.keys(obj)[obj_count]] = text;
      $(this).children('input').remove()
      $(this).text(text)
      obj_count++
    }
   })
    $(this).before('<button class="btn edit">Edit</button>')
    $(this).remove()
    $('#example').find('tr').each(function(){
    })
    $.ajax({
      type:'post',
      url: '/edit_users',
      data:{'_token':'<?php echo csrf_token() ?>',user_data:obj},
    })
 });

 $(document).on('click', '.delete', function() {
   let id = $(this).parents('tr').first().text()
    $.ajax({
      type:'post',
      url: '/delete_users',
      data:{'_token':'<?php echo csrf_token() ?>',user_id:id}
    })
   $(this).parents('tr').remove()
 });
} );
        
</script>
@endsection