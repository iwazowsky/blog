<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Auth;


class EditController extends Controller
{



    /**
     * @var string
     */
    protected $redirectTo = '/editor_page';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(Request $request)
    {   $data =  $request->all();
        $data['remember_token'] = $data['_token'];
        unset($data['_token']);
        $v = Validator::make($data,[
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'email' => ['required','email','unique:users,email,'.Auth::user()->id],
            'username' => 'required|string|unique:users,username,'.Auth::user()->id,
            'password' => 'string|min:6|confirmed|nullable',
            'birthdate' => 'date',
        ]);
         if ($v->fails())
            return redirect()->back()->withErrors($v->errors());
        else {
            $this->update(Auth::user()->id,$data);
            return redirect('/editor_page');
            
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function update($id,array $data)
    {   $data = array_filter($data);
        if(isset($data['password'])) 
            $data['password'] = Hash::make($data['password']);
        User::whereId($id)->update($data);
    }
}
