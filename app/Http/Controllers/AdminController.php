<?php 
	namespace App\Http\Controllers;
	use App\User;
	use Illuminate\Support\Facades\Validator;
	Class AdminController extends Controller
	{
		public function get_users()
		{	
			$users = User::all();
			foreach ($users as $user ) {
				$user['edit'] = "<button class='btn edit'>Edit</button>";
				$user['delete'] = "<button class='btn delete'>Delete</button>";
			}
			echo json_encode($users);
		}
		public function admin_users()
		{	
			return view('/users');
		}
		public function edit_users()
		{	$data = $_POST['user_data'];
			$user_id = $data['id'];
			$data['profpic'] = $data['profile picture'];
			unset($data['profile picture']);
			$data['type'] = $data['status'];
			unset($data['status']);
			unset($data['id']);
			unset($data['created']);
			unset($data['last updated']);
			$v =  Validator::make($data, [
	            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'email' => ['required','email','unique:users,email,'.$user_id],
            'username' => 'required|string|unique:users,username,'.$user_id,
            'password' => 'string|min:6|confirmed|nullable',
            'birthdate' => 'date',
        	]);
        	  if ($v->fails())
            	echo $v->errors();
        	else {
          	  $data = array_filter($data);
        	  User::whereId($user_id)->update($data); 
	        }
		}
		public function delete_user()
		{	
			$id = $_POST['user_id'];
			User::find($id)->delete();	
		}
	}
 ?>