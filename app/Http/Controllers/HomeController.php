<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    public function admin_page()
    { 
     if(Auth::user()->type == 'admin')
        return redirect ('admin/users');
     else 
       return redirect()->back();
    }
    public function edit_info()
    {
        return view('/editor_page');
    }
    public function upload_profpic(Request $request) {
         $tmp = $request->file('profpic')->getPathName();
         $name = $request->file('profpic')->getClientOriginalName();
         $destination = "images/".uniqid().$name;
         $user_id = $request->input('user-id');
         move_uploaded_file($tmp, $destination);
        $edited_character = DB::table('users')->where('id',$user_id)->update([
            'profpic' => $destination
         ]);
        return redirect()->back();
    }
}
